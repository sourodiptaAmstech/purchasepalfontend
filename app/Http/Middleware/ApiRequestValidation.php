<?php

namespace App\Http\Middleware;


use Closure;
use App\Helper\Helper;
use Exception;

class ApiRequestValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
 /*   public function handle($request, Closure $next)
    {
        $token=$request->header("public_key");
        $token=explode(":::",$token);
        if(empty($token)){
            return response()->json(["key"=>Helper::enCodeAESData(json_encode(array("msg"=>"Access Denied","reason"=>"PUBLIC_KEY_REQUIRED")))], 401, [], JSON_PRETTY_PRINT);
        }
        else{
            if(Helper::timeDiffInMin($token[1])>env("PUBLIC_KEY_EXPIRY_TIME")){
                return response()->json(["key"=>Helper::enCodeAESData(json_encode(array("msg"=>"Access Denied","reason"=>"PUBLIC_KEY_TIME_EXPIRED")))], 401, [], JSON_PRETTY_PRINT);
            }
            elseif(env("APP_KEY")!=="base64:".$token[0]){
                return response()->json(["key"=>Helper::enCodeAESData(json_encode(array("msg"=>"Access Denied","reason"=>"PUBLIC_KEY_INVALID")))], 401, [], JSON_PRETTY_PRINT);
            }
        }
        return $next($request);
    }
    */
    public function handle($request, Closure $next)
    {//return $next($request);
        try{
            $token="";
            $temp="base64:aoiDOtCqrFLsvM7Opx1Mxvv2wshdL8NyupsPQJYJRh4=";
            $temp2=10;
            foreach (getallheaders() as $name => $value) {
                if($name=="public_key"){
                    $token=$value;
                }
            }
            if($token!=="")
            $token=explode(":::",$token);
            if(empty($token)){
                return response()->json(["key"=>Helper::enCodeAESData(json_encode(array("msg"=>"Access Denied","reason"=>"PUBLIC_KEY_REQUIRED","code"=>401)))], 401, [], JSON_PRETTY_PRINT);
            }
         /*   else{

                if(Helper::timeDiffInMin($token[1])>$temp2){
                    return response()->json(["key"=>Helper::enCodeAESData(json_encode(array("msg"=>"Access Denied","reason"=>"PUBLIC_KEY_TIME_EXPIRED","code"=>401)))], 401, [], JSON_PRETTY_PRINT);
                }
                elseif($temp!=="base64:".$token[0]){
                    return response()->json(["key"=>Helper::enCodeAESData(json_encode(array("msg"=>"Access Denied","reason"=>"PUBLIC_KEY_INVALID","code"=>401)))], 401, [], JSON_PRETTY_PRINT);
                }
            }
            */
            return $next($request);
        }
        catch(Exception $e){
            return response()->json(["key"=>Helper::enCodeAESData(json_encode(array("msg"=>"Access Denied","reason"=>"PUBLIC_KEY_REQUIRED","code"=>401)))], 401, [], JSON_PRETTY_PRINT);
        }
    }
}
