<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Validator;

use App\Helper\Helper;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function ReturnEncriptedData(array $data){
     return Helper::enCodeAESData(json_encode($data));
     return $data;
    }
    protected function ReturnDecryptedData($data){
        $data= Helper::deCodeAESData($data);
        $data=json_decode($data,true);
        return $data;
    }
    protected function requestValidation($request,$rule){
        $validator =  Validator::make($request,$rule);
        if ($validator->fails()) {
            $array=$validator->getMessageBag()->toArray();
            foreach($array as $key=>$val){
                return (object)['message'=>$val[0],"field"=>$key,"status"=>"false"];
            }
        }
        return (object)['message'=>null,"field"=>null,"status"=>"true"];;

    }
}
