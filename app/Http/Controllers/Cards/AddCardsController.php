<?php

namespace App\Http\Controllers\Cards;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\RestClient;
use Exception;

class AddCardsController extends Controller
{
    private $externalServerURL;
    public function __construct(Request $request){
        $this->externalServerURL=env("PRIVER_SERVER_URL");
    }
    function add(Request $request){
        $request['timeZone']=$timeZone=$request->header("timeZone");
        $rule=[
            'data' =>'required' ,      
            'timeZone'=>'required'
        ];
        $validator=$this->requestValidation($request->all(),$rule);
        if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
        $data=$this->ReturnDecryptedData($request->data);
        // validating incoming data
        $rule=[
            'memo' => 'required',
            'funding_token' => 'required',
        ];
        $validator=$this->requestValidation($data,$rule);
        if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
        $data['type']="UNLOCKED";        
        $returnData=["code" => 400, "status" => "error", "message" => ""];
        $requestdata    = $data;
        $requestURL     = "https://sandbox.privacy.com/v1/card";
        $requestType    = 'POST';
        $header         = array( "Authorization: api-key e805b1af-9ca4-40af-bf80-150a0847dd5e",
        "Content-Type: application/json");

        $returnData       = RestClient::curlPrivacy($requestURL, json_encode($requestdata), $header, $requestType);
        
        if($returnData['httpcode']=="200" || $returnData['httpcode']=="201" || $returnData['httpcode']==200 || $returnData['httpcode']==201){
            
            $cardObject=json_decode($returnData['objResponse']);
          //  print_r($cardObject); exit;
            $returnableCardSave=[
                "created"=>$cardObject->created,
                "exp_month"=>$cardObject->exp_month,
                "exp_year"=>$cardObject->exp_year,
                "last_four"=>$cardObject->last_four,
                "memo"=>$cardObject->memo,
                "state"=>$cardObject->state,
                "token"=>$cardObject->token,
                "type"=>$cardObject->type
            ];
            $returnableCard=[
                "created"=>$cardObject->created,
                "exp_month"=>$cardObject->exp_month,
                "exp_year"=>$cardObject->exp_year,
                "last_four"=>$cardObject->last_four,
                "memo"=>$cardObject->memo,
                "state"=>$cardObject->state
              //  "token"=>$cardObject->token,
                //"type"=>$cardObject->type
            ];
            // Added the saved bank to the db 
            $resData=['message'=>"Card is created successfully","data"=>(object)$returnableCard,"errors"=>array("exception"=>["Everything OK"],"e"=>[])];
            
            return response()->json(["key"=>$this->ReturnEncriptedData($resData)], $returnData['httpcode'], [], JSON_PRETTY_PRINT);
           
            //return response(['message'=>"Card is created successfully.","data"=>(object)$returnableCard,"errors"=>array("exception"=>["Everything OK"],"e"=>[])],$returnData['httpcode']);
        }
        else{
            $resData=['message'=>"Somenting went wrong!","field"=>"Privacy Unable to Process","data"=>(object)json_decode($returnData['objResponse']),"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])];
            return response()->json(["key"=>$this->ReturnEncriptedData($resData)], $returnData['httpcode'], [], JSON_PRETTY_PRINT);
        }
    }
}