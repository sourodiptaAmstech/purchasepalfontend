<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Helper\RestClient;
use App\Notifications\email;
use Exception;
use Illuminate\Support\Facades\Notification;

class RegistrationServices extends Controller
{
    //
    private $externalServerURL;
    public function __construct(Request $request){
        $this->externalServerURL=env("PRIVER_SERVER_URL");
    }


    public function test (Request $request) {
        return $request->user();
    }
    public function getPreRegistrationData(Request $request){
        // get country list from miroservices

        $returnData=["code" => 400, "status" => "error", "message" => ""];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/country";
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");

        $returnData       = RestClient::curlRequest($requestURL, '', $header, $requestType);
        $returnData=json_decode($returnData,true);





        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }
    public function country(Request $request){
        // get country list from miroservices
        $returnData=["code" => 400, "status" => "error", "message" => ""];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/country";
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");
        $returnData       = RestClient::curlRequest($requestURL, '', $header, $requestType);
        $returnData=json_decode($returnData,true);
        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }


    public function countrybyid(Request $request){
        // get country list from miroservices
        $data=$request->header("data");
        $data=$this->ReturnDecryptedData($data);
        $returnData=["code" => 400, "status" => "error", "message" => "Hello"];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/country/".$data['id'];
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");

        $returnData       = RestClient::curlRequest($requestURL, '', $header, $requestType);
        $returnData=json_decode($returnData,true);





        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }
    public function state(Request $request){
        // get country list from miroservices
        $returnData=["code" => 400, "status" => "error", "message" => "Hello"];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/state";
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");

        $returnData       = RestClient::curlRequest($requestURL, '', $header, $requestType);
        $returnData=json_decode($returnData,true);





        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }
    public function statebyid(Request $request){
        // get country list from miroservices
        $data=$request->header("data");
        $data=$this->ReturnDecryptedData($data);
        $returnData=["code" => 400, "status" => "error", "message" => "Hello"];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/state/".$data['id'];
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");

        $returnData       = RestClient::curlRequest($requestURL, '', $header, $requestType);
        $returnData=json_decode($returnData,true);





        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }
    public function statebycountryid(Request $request){
        // get country list from miroservices
        $data=$request->header("data");
        $data=$this->ReturnDecryptedData($data);
        $returnData=["code" => 400, "status" => "error", "message" => "Hello"];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/state/country/".$data['id'];
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");

        $returnData       = RestClient::curlRequest($requestURL, '', $header, $requestType);
        $returnData=json_decode($returnData,true);





        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }
    public function city(Request $request){
        // get country list from miroservices
      //  $id=$request['id'];
        $returnData=["code" => 400, "status" => "error", "message" => "Hello"];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/city";
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");

        $returnData       = RestClient::curlRequest($requestURL, '', $header, $requestType);
        $returnData=json_decode($returnData,true);





        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }
    public function citybyid(Request $request){
        // get country list from miroservices
        $data=$request->header("data");
        $data=$this->ReturnDecryptedData($data);
        $returnData=["code" => 400, "status" => "error", "message" => "Hello"];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/city/".$data['id'];
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");

        $returnData       = RestClient::curlRequest($requestURL, '', $header, $requestType);
        $returnData=json_decode($returnData,true);





        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }
    public function citybystateid(Request $request){
        // get country list from miroservices
        $data=$request->header("data");
        $data=$this->ReturnDecryptedData($data);
        $returnData=["code" => 400, "status" => "error", "message" => "Hello"];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/city/state/".$data['id'];
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");
        $returnData       = RestClient::curlRequest($requestURL, '', $header, $requestType);
        $returnData=json_decode($returnData,true);
        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }

    public function saveRegisterationData(Request $request){
        // get encoded data from the header
        try{
            $data=$request->header("data");
            $data=$this->ReturnDecryptedData($data);
            $returnData=["code" => 400, "status" => "error", "message" => "Your request could not be process."];
            $error=false; $msg="";$requestdata=array();
            $requestdata=array('user_fname'=>"",'user_mname'=>"",'user_lname'=>"",'user_email'=>"",'user_phone'=>"",'user_password'=>"",
            'user_photo'=>"",'user_dob'=>"",'user_address_line_one'=>"",'user_address_line_two'=>"",'country_id'=>"",'states_id'=>"",
            'city_id'=>"",'user_zip_code'=>"","phoneCode"=>"");
            foreach($data as $key=>$val){
                switch($key){
                    case 'firstName':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['firstName']="First name is required.";
                        }
                        else{
                            $requestdata['user_fname']=trim($val);
                        }
                    break;
                    case 'middleName':
                        $requestdata['user_mname']=$val;
                    break;
                    case 'lastName':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['lastName']="Last name is required.";
                        }
                        else{
                            $requestdata['user_lname']=$val;
                        }
                    break;
                    case 'email':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['email']="Email ID is required.";
                        }
                        else{
                            // check for valid email
                            $requestdata['user_email']=$val;
                        }
                    break;
                    case 'phone':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['phone']="Phone Number is required.";
                        }
                        else{

                            $requestdata['user_phone']=$val;
                        }
                    break;
                    case 'password':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['password']="Password is required.";
                        }
                        else{
                            $requestdata['user_password']=MD5($val);
                        }
                    break;
                    case 'dateOfBirth':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['dateOfBirth']="Date of Birth is required.";
                        }
                        else{
                            $requestdata['user_dob']=$val;
                        }

                    break;
                    case 'addressLineOne':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['addressLineOne']="Address is required.";
                        }
                        else{
                            $requestdata['user_address_line_one']=trim($val);
                        }
                    break;
                    case 'addressLineTwo':
                        $requestdata['user_address_line_two']=trim($val);
                    break;
                    case 'countryId':
                        if((int)$val===0){
                            $error=true;
                            $msg['countryId']="Country is required.";
                        }
                        else{
                            $requestdata['country_id']=(int)$val;
                        }
                    break;
                    case 'statesId':
                        if((int)$val===0){
                            $error=true;
                            $msg['statesId']="States is required.";
                        }
                        else{
                            $requestdata['states_id']=(int)$val;
                        }
                    break;
                    case 'cityId':
                        if((int)$val===0){
                            $error=true;
                            $msg['cityId']="City is required.";
                        }
                        else{
                            $requestdata['city_id']=(int)$val;
                        }
                    break;
                    case 'zipCode':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['zipCode']="Zip/Postal code is required.";
                        }
                        else{
                            $requestdata['user_zip_code']=trim($val);
                        }
                    break;
                    case 'phoneCode':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['phoneCode']="country code is required.";
                        }
                        else{
                            $requestdata['phoneCode']=trim($val);
                        }
                    break;
                }
            }
            if($error===true )
                {
                    $returnData=["code" => 400, "status" => "error", "message" => $msg];
                    return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
                }
                else{
                    // return response()->json(array(), 201, [], JSON_PRETTY_PRINT);;
                   // $requestdata    = $requestdata;
                    $requestdata=$requestdata;
                    $requestURL     = "http://10.139.24.24/api/registration";
                    $requestType    = 'POST';
                    $header         = array("content-type: application/json","auth:null","Accept:application/json");
                    $returnData       = RestClient::curlRequest($requestURL,json_encode($requestdata), $header, $requestType);
                    $returnDatas=json_decode($returnData,true);
                    if(array_key_exists("status",$returnDatas)){
                        if((int)$returnDatas['status']===201){
                            $emailData=array(
                                "lineOne"=>"You are receiving this email for verification of your email account. Your OTP is",
                                "lineTwo"=>"If you did not request a registration with us, no further action is required."
                            );
                            Notification::route('mail', $requestdata['user_email'])->notify(new email((int)$returnDatas['otp'],$emailData));
                            $returnData=["code" => (int)$returnDatas['status'], "status" => "success", "message" => "OTP sent to your registered email id","data"=>(object)["otp"=>(int)$returnDatas['otp']] ];
                            return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], (int)$returnDatas['status'], [], JSON_PRETTY_PRINT);
                        }
                        else
                        {
                            $returnData=["code" => (int)$returnDatas['status'], "status" => "error", "message" => "Email id did not match. Please Try again.", "data"=>(object)$returnDatas];
                            return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], $returnDatas['status'], [], JSON_PRETTY_PRINT);
                        }
                    }
                    else{
                        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
                    }
                    return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
                }
                //     return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
            }
            catch (Exception $e) {
                return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
            }
        }
        public function login(Request $request){
        try{
            $data=$request->header("data");
            $data=$this->ReturnDecryptedData($data);
            $error=false; $msg="";
            $returnData=["code" => 400, "status" => "error", "message" => "Your request could not be process.", "data"=>(object)array()];
            $requestdata=array('user_email'=>"",'user_password'=>"");
            foreach($data as $key=>$val){
                switch($key){
                    case 'email':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['email']="Email ID is required.";
                        }
                        else{
                            // check for valid email
                            $requestdata['user_email']=$val;
                        }
                    break;
                    case 'password':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['password']="Password is required.";
                        }
                        else{
                            $requestdata['user_password']=MD5($val);
                        }
                    break;
                }
            }
            if($error===true)
            {
                $returnData=["code" => 400, "status" => "error", "message" => $msg,"data"=>(object)array()];
                return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
            }
            else{
                $requestdata=$requestdata;
                $requestURL     = "http://10.139.24.24/api/login";
                $requestType    = 'POST';
                $header         = array("content-type: application/json","auth:null","Accept:application/json");
                $returnDatas       = RestClient::curlRequest($requestURL,json_encode($requestdata), $header, $requestType);
                $returnDatas=json_decode($returnDatas,true);

                if(array_key_exists("status",$returnDatas)){
                    if((int)$returnDatas['status']===200){
                        $token=$this->ReturnEncriptedData(array("passKey"=>$returnDatas['data'][0]['user_password'],"time"=>strtotime(date("Y-m-d H:i:s"))));
                        $returnData=["code" => 200, "status" => "success", "message" => "You loged in","data"=>["token"=>$token, "user_id"=>$returnDatas['data'][0]['user_id']] ];
                        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 200, [], JSON_PRETTY_PRINT);
                    }
                    else  if((int)$returnDatas['status']===403){
                        $emailData=array(
                            "lineOne"=>"You are receiving this email for verification of your email account. Your OTP is",
                            "lineTwo"=>"If you did not request a registration with us, no further action is required."
                        );
                        Notification::route('mail', $requestdata['user_email'])->notify(new email((int)$returnDatas['otp'],$emailData));
                        $returnData=["code" => $returnDatas['status'], "status" => "forbidden", "message" =>$returnDatas['msg'],"data"=>(object)array() ];
                        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], (int)$returnDatas['status'], [], JSON_PRETTY_PRINT);
                    }
                    else
                    {
                        $returnData=["code" => 401, "status" => "error", "message" => "Credentials did not match. Please Try again.", "data"=>(object)array()];
                        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 401, [], JSON_PRETTY_PRINT);
                    }
                }
                else{
                    return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
                }

            }
        }
        catch (Exception $e) {
            return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
        }
    }
    public function userProfile(Request $request){
        // get country list from miroservices
        $data=$request->header("data");
        $data=$this->ReturnDecryptedData($data);
        $returnData=["code" => 400, "status" => "error", "message" => "error","data"=>[]];
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/profile";
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");
        $returnDatas       = RestClient::curlRequest($requestURL, json_encode($data), $header, $requestType);
        $returnDatas=json_decode($returnDatas,true);
        if(array_key_exists("status",$returnDatas)){
            if((int)$returnDatas['status']===200){
                $returnData["code"]=200;
                $returnData["status"]="success";
                foreach($returnDatas['data'] as $key=>$val){
               //    print_r($key);
                    switch($key){
                        case 'user_fname':
                            $returnData['data']['firstName']=trim($val);
                            break;
                        case 'user_mname':
                            $returnData['data']['middleName']=trim($val);;
                            break;
                        case 'user_lname':
                            $returnData['data']['lastName']=trim($val);
                            break;
                        case 'user_email':
                            $returnData['data']['email']=trim($val);
                            break;
                        case 'user_phone':
                            $returnData['data']['phone']=trim($val);
                            break;
                        case 'user_dob':
                            $returnData['data']['dateOfBirth']=trim($val);
                                break;
                        case 'user_address_line_one':
                            $returnData['data']['addressLineOne']=trim($val);
                            break;
                        case 'user_address_line_two':
                            $returnData['data']['addressLineTwo']=trim($val);
                            break;
                        case 'country_id':
                            $returnData['data']['countryId']=(int)$val;
                            break;
                        case 'states_id':
                            $returnData['data']['statesId']=(int)$val;
                            break;
                        case 'city_id':
                            $returnData['data']['cityId']=(int)$val;
                            break;
                        case 'user_zip_code':
                            $returnData['data']['zipCode']=trim($val);
                            break;
                    }
                }
            }
        }

        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 201, [], JSON_PRETTY_PRINT);
    }




    public function forgetPassword(Request $request){
        try{
            $data=$request->header("data");
            $data=$this->ReturnDecryptedData($data);

            $error=false; $msg="";
            $returnData=["code" => 400, "status" => "error", "message" => "Your request could not be process.", "data"=>(object)array()];
            $requestdata=array('user_email'=>"");
            foreach($data as $key=>$val){
                switch($key){
                    case 'email':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['email']="Email ID is required.";
                        }
                        else{
                            // check for valid email
                            $requestdata['user_email']=$val;
                        }
                        break;
                }
            }
            if($error===true)
            {
                $returnData=["code" => 400, "status" => "error", "message" => $msg,"data"=>(object)array()];
                return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
            }
            else{
                $requestdata=$requestdata;
                $requestURL     = "http://10.139.24.24/api/forgetpass";
                $requestType    = 'GET';
                $header         = array("content-type: application/json","auth:null","Accept:application/json");
                $returnDatas       = RestClient::curlRequest($requestURL,json_encode($requestdata), $header, $requestType);
                $returnDatas=json_decode($returnDatas,true);
                if(array_key_exists("status",$returnDatas)){
                    if((int)$returnDatas['status']===200){
                        $emailData=array(
                            "lineOne"=>"You are receiving this email because we received a password reset request for your account. Your OTP is",
                            "lineTwo"=>"If you did not request a password reset, no further action is required."
                        );
                        Notification::route('mail', $requestdata['user_email'])->notify(new email((int)$returnDatas['otp'],$emailData));
                        $returnData=["code" => 200, "status" => "success", "message" => "OTP sent to your email id","data"=>(object)["otp"=>(int)$returnDatas['otp']] ];
                        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 200, [], JSON_PRETTY_PRINT);
                    }
                    else
                    {
                        $returnData=["code" => 401, "status" => "error", "message" => "Email id did not match. Please Try again.", "data"=>(object)array()];
                        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 401, [], JSON_PRETTY_PRINT);
                    }
                }
                else{
                    return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
                }

            }
        }
        catch (Exception $e) {
            return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
        }
    }
    public function saveAndVerifyOtpForgetPassword(Request $request){
        $returnData=["code" => 400, "status" => "error", "message" => "Your request could not be process.", "data"=>(object)array()];
        try{
            $data=$request->header("data");
            $data=$this->ReturnDecryptedData($data);
            $error=false; $msg="";
            $requestdata=array('user_email'=>"",'user_platform_otp'=>"","user_password"=>"");
            foreach($data as $key=>$val){
                switch($key){
                    case 'email':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['email']="Email ID is required.";
                        }
                        else{
                            // check for valid email
                            $requestdata['user_email']=$val;
                        }
                    break;
                    case 'otp':
                        if(strlen(trim($val))===0 && (int)$val!==0){
                            $error=true;
                            $msg['otp']="OTP is required.";
                        }
                        else{
                            $requestdata['user_platform_otp']=$val;
                        }
                    break;
                    case 'password':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['password']="Password is required.";
                        }
                        else{
                            $requestdata['user_password']=$val;
                        }
                    break;
                }
            }
            if($error===true)
            {
                $returnData=["code" => 400, "status" => "error", "message" => $msg,"data"=>(object)array()];
                return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
            }
            else{
                $requestdata=$requestdata;
                $requestURL     = "http://10.139.24.24/api/forgetpass";
                $requestType    = 'POST';
                $header         = array("content-type: application/json","auth:null","Accept:application/json");
                $returnDatas       = RestClient::curlRequest($requestURL,json_encode($requestdata), $header, $requestType);
                $returnDatas=json_decode($returnDatas,true);
               // print_r($returnDatas); exit;
               if(array_key_exists("status",$returnDatas)){

                        $returnData=["code" => $returnDatas['status'], "status" => "error", "message" => $returnDatas['msg'], "data"=>(object)[]];
                        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], $returnDatas['status'], [], JSON_PRETTY_PRINT);

                }
                else{
                    return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
                }

            }
        }
        catch(Exception $e){
            return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);

        }
    }

    public function changepassword(Request $request){
        $data=$request->header("data");
        $data=$this->ReturnDecryptedData($data);
        //print_r($data); exit;
        $error=false; $msg="";
        $returnData=["code" => 400, "status" => "error", "message" => "Your request could not be process.", "data"=>(object)array()];
        $requestdata=array('user_id'=>"",'old_password'=>"",'new_password'=>"");
        foreach($data as $key=>$val){
            switch($key){
                case 'user_id':
                    if(strlen(trim($val))===0){
                        $error=true;
                        $msg['user_id']="User ID is required.";
                    }
                    else{
                        // check for valid email
                        $requestdata['user_id']=$val;
                    }
                break;
                case 'old_password':
                    if(strlen(trim($val))===0){
                        $error=true;
                        $msg['old_password']="Old password is required.";
                    }
                    else{
                        $requestdata['old_password']=MD5($val);
                    }
                break;
                case 'new_password':
                    if(strlen(trim($val))===0){
                        $error=true;
                        $msg['new_password']="New password is required.";
                    }
                    else{
                        $requestdata['new_password']=MD5($val);
                    }
                break;
            }
        }
        if($error===true)
        {
            $returnData=["code" => 400, "status" => "error", "message" => $msg,"data"=>(object)array()];
            return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
        }
        else{
            $requestdata=$requestdata;
            $requestURL     = "http://10.139.24.24/api/changepassword";
            $requestType    = 'POST';
            $header         = array("content-type: application/json","auth:null","Accept:application/json");
            $returnDatas       = RestClient::curlRequest($requestURL,json_encode($requestdata), $header, $requestType);
            $returnDatas=json_decode($returnDatas,true);
            //print_r($returnDatas); exit;
            if(array_key_exists("status",$returnDatas)){
                $returnData=["code" => $returnDatas['status'], "status" => "success", "message" => $returnDatas['msg'], "data"=>(object)[]];
                return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], $returnDatas['status'], [], JSON_PRETTY_PRINT);
            }
            else{
                return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
            }
        }
    }

    public function otpVeificationEmail(Request $request){
        $returnData=["code" => 400, "status" => "error", "message" => "Your request could not be process.", "data"=>(object)array()];
        try{
            $data=$request->header("data");
            $data=$this->ReturnDecryptedData($data);
            $error=false; $msg="";
            $requestdata=array('user_email'=>"",'user_platform_otp'=>"");
            foreach($data as $key=>$val){
                switch($key){
                    case 'email':
                        if(strlen(trim($val))===0){
                            $error=true;
                            $msg['email']="Email ID is required.";
                        }
                        else{
                            // check for valid email
                            $requestdata['user_email']=$val;
                        }
                    break;
                    case 'otp':
                        if(strlen(trim($val))===0 && (int)$val!==0){
                            $error=true;
                            $msg['otp']="OTP is required.";
                        }
                        else{
                            $requestdata['user_platform_otp']=$val;
                        }
                    break;

                }
            }
            if($error===true)
            {
                $returnData=["code" => 400, "status" => "error", "message" => $msg,"data"=>(object)array()];
                return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
            }
            else{
                $requestdata=$requestdata;
                $requestURL     = "http://10.139.24.24/api/otpveificationemail";
                $requestType    = 'POST';
                $header         = array("content-type: application/json","auth:null","Accept:application/json");
                $returnDatas       = RestClient::curlRequest($requestURL,json_encode($requestdata), $header, $requestType);
                $returnDatas=json_decode($returnDatas,true);
               // print_r($returnDatas); exit;
               if(array_key_exists("status",$returnDatas)){
                   $returnData=["code" => $returnDatas['status'], "status" => "error", "message" => $returnDatas['msg'], "data"=>(object)[]];
                   return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], $returnDatas['status'], [], JSON_PRETTY_PRINT);
                }
                else{
                    return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
                }
            }


        }
        catch(Exception $e){
            return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], 400, [], JSON_PRETTY_PRINT);
        }

    }






}
