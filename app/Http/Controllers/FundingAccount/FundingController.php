<?php

namespace App\Http\Controllers\FundingAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\RestClient;
use Exception;

class FundingController extends Controller
{

    // Add Bank as funding account 
    private $externalServerURL;
    public function __construct(Request $request){
      //  echo "i am in cons"; exit;
        $this->externalServerURL=env("PRIVER_SERVER_URL");
    }
    function addBank(Request $request){    
      //  echo "i am in"; exit;
        $request['timeZone']=$timeZone=$request->header("timeZone");
        $request['data']=$timeZone=$request->header("data");
        $rule=[
            'data' =>'required',
            'timeZone'=>'required'
        ];
        $validator=$this->requestValidation($request->all(),$rule);
        if($validator->status=="false"){
            $resData=['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])];
            return response()->json(["key"=>$this->ReturnEncriptedData($resData)], 422, [], JSON_PRETTY_PRINT);
        };
        $data=$this->ReturnDecryptedData($request->data);
        // validating incoming data
        $rule=[
            'routing_number' => 'required',
            'account_number' => 'required',
            'account_name' => 'required',
            'user_id'=>'required',
            'bank_name'=>'required'
        ];
        $validator=$this->requestValidation($data,$rule);
        if($validator->status=="false"){ 
            $resData=['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])];
            return response()->json(["key"=>$this->ReturnEncriptedData($resData)], 422, [], JSON_PRETTY_PRINT);
        
        };
        

        $returnData=["code" => 400, "status" => "error", "message" => ""];
        $requestdata    = ['routing_number'=>$data['routing_number'],'account_number'=>$data['account_number'],'account_name'=>$data['account_name']];
        $requestURL     = "https://sandbox.privacy.com/v1/fundingsource/bank";
        $requestType    = 'POST';
        $header         = array( "Authorization: api-key e805b1af-9ca4-40af-bf80-150a0847dd5e",
        "Content-Type: application/json");

        $returnData       = RestClient::curlPrivacy($requestURL, json_encode($requestdata), $header, $requestType);
        
        if($returnData['httpcode']=="200" || $returnData['httpcode']=="201" || $returnData['httpcode']==200 || $returnData['httpcode']==201){
            // Added the saved bank to the db 
          //  print_r(json_decode($returnData['objResponse'],true));
            $objResponse=json_decode($returnData['objResponse'],true);
            $objResponse=$objResponse['data'];
            $objResponse['user_id']=$data['user_id'];
            $objResponse['scouce_name']=$data['bank_name'];
            $objResponse['source_type']="Bank";
            

            //$requestdata=$requestdata;
            $requestURL     = "http://10.139.24.24/api/save/bank";
            $requestType    = 'POST';
            $header         = array("content-type: application/json","auth:null","Accept:application/json");
            $returnDatas       = RestClient::curlRequest($requestURL,json_encode($objResponse), $header, $requestType);
            $returnDatas=json_decode($returnDatas,true);
          //  print_r($returnDatas); exit;
            
           // exit;




            $resData=['message'=>"Your Bank is added!","data"=>(object)[],"errors"=>array("exception"=>["Everything OK"],"e"=>[])];
            return response()->json(["key"=>$this->ReturnEncriptedData($resData)], $returnData['httpcode'], [], JSON_PRETTY_PRINT);
           
            //return response(['message'=>"Your Bank is added!","data"=>(object)json_decode($returnData['objResponse']),"errors"=>array("exception"=>["Everything OK"],"e"=>[])],$returnData['httpcode']);
        }
        else{
            $resData=['message'=>"Somenting went wrong!","field"=>"Privacy Unable to Process","data"=>(object)json_decode($returnData['objResponse']),"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])];
            return response()->json(["key"=>$this->ReturnEncriptedData($resData)], $returnData['httpcode'], [], JSON_PRETTY_PRINT);
        }
    }

    function getBank(Request $request){
        $request['timeZone']=$timeZone=$request->header("timeZone");
        $request['data']=$timeZone=$request->header("data");
        $rule=[
            'data' =>'required' ,      
            'timeZone'=>'required'
        ];
        $validator=$this->requestValidation($request->all(),$rule);
        if($validator->status=="false"){ 
            $resData=['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])];
            return response()->json(["key"=>$this->ReturnEncriptedData($resData)], 422, [], JSON_PRETTY_PRINT);
            
         };
        $data=$this->ReturnDecryptedData($request->data);
        // validating incoming data
        $rule=[            
            'user_id'=>'required',
        ];
        $validator=$this->requestValidation($data,$rule);
        if($validator->status=="false"){ 
            $resData=['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])];
            return response()->json(["key"=>$this->ReturnEncriptedData($resData)], 422, [], JSON_PRETTY_PRINT);
        };
        $returnData=['message'=>"Bad Request","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"e"=>[])];
        $status=400;
           
       
        $requestdata    = array();
        $requestURL     = "http://10.139.24.24/api/get/bank";
        $requestType    = 'GET';
        $header         = array("content-type: application/json","auth:null","Accept:application/json");
        $returnDatas       = RestClient::curlRequest($requestURL, json_encode($data), $header, $requestType);
        $returnDatas=json_decode($returnDatas,true);
       
        if(array_key_exists("status",$returnDatas)){
            if((int)$returnDatas['status']===200){
                $returnData=['message'=>"Record Found","data"=>$returnDatas['data'],"errors"=>array("exception"=>["Record Found"],"e"=>[])];
                $status=200;
            }
            else{
                $returnData=['message'=>"Record Not Found","data"=>(object)[],"errors"=>array("exception"=>["Record Not Found"],"e"=>[])];
                $status=404;
            }
        }
        return response()->json(["key"=>$this->ReturnEncriptedData($returnData)], $status, [], JSON_PRETTY_PRINT);
    }
}