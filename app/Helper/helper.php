<?php


namespace App\Helper;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Boolean;

class Helper
{

    public static function timeDiffInMin($time){
        $time=date('Y-m-d h:i:s',$time);
        $startTime = Carbon::parse($time);
        $finishTime = Carbon::now();
       // echo $finishTime->diffInMinutes($startTime); exit;
        return $finishTime->diffInMinutes($startTime);
    }


    /*** De-code AES */

    public static function deCodeAESData($data){
        $ct= (openssl_decrypt($data, 'aes-256-cbc', "Aa7F6d7F9cF0bEb238dBbE0d715E3d6B", 0, "9bAd74D61e7E03bB"));
      //  $pt = openssl_decrypt(base64_decode("me2XTeWxwAslnSG8XnvWCFv4JKwTIrC9tbnX8gXXSn6yPSrD8o/qd1/6zfTBo2Ss6Br7YfCfoce5ULxpprbk7g=="), 'AES-256-CBC',"Aa7F6d7F9cF0bEb238dBbE0d715E3d6B", 0, "9bAd74D61e7E03bB");
        return $ct;
    }
/** en-code */
    public static function enCodeAESData($data){
        $ct= (openssl_encrypt($data, 'aes-256-cbc', "Aa7F6d7F9cF0bEb238dBbE0d715E3d6B", 0, "9bAd74D61e7E03bB"));
      //  $pt = openssl_decrypt(base64_decode("me2XTeWxwAslnSG8XnvWCFv4JKwTIrC9tbnX8gXXSn6yPSrD8o/qd1/6zfTBo2Ss6Br7YfCfoce5ULxpprbk7g=="), 'AES-256-CBC',"Aa7F6d7F9cF0bEb238dBbE0d715E3d6B", 0, "9bAd74D61e7E03bB");
        return $ct;
    }

    /**
* Helper library for CryptoJS AES encryption/decryption
* Allow you to use AES encryption on client side and server side vice versa
*
* @author BrainFooLong (bfldev.com)
* @link https://github.com/brainfoolong/cryptojs-aes-php
*/
/**
* Decrypt data from a CryptoJS json encoding string
*
* @param mixed $passphrase
* @param mixed $jsonString
* @return mixed
*/
public static function cryptoJsAesDecrypt($jsonString){
    $passphrase="WelcometoeTWIST";
    $jsondata = json_decode($jsonString, true);
    try {
        $salt = hex2bin($jsondata["s"]);
        $iv  = hex2bin($jsondata["iv"]);
    } catch(Exception $e) { return null; }
    $ct = base64_decode($jsondata["ct"]);
    $concatedPassphrase = $passphrase.$salt;
    $md5 = array();
    $md5[0] = md5($concatedPassphrase, true);
    $result = $md5[0];
    for ($i = 1; $i < 3; $i++) {
        $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
        $result .= $md5[$i];
    }
    $key = substr($result, 0, 32);
    $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);

    return json_decode($data, true);
}
/**
* Encrypt value to a cryptojs compatiable json encoding string
*
* @param mixed $passphrase
* @param mixed $value
* @return string
*/
public static function cryptoJsAesEncrypt($value){
    $passphrase="WelcometoeTWIST";
    $salt = openssl_random_pseudo_bytes(8);
    $salted = '';
    $dx = '';
    while (strlen($salted) < 48) {
        $dx = md5($dx.$passphrase.$salt, true);
        $salted .= $dx;
    }
    $key = substr($salted, 0, 32);
    $iv  = substr($salted, 32,16);
    $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
    $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
    return json_encode($data);
}


public static function decodeForcryptoJsAesDecrypt($requestd_data){
    $a= explode("=*fcc2b4c5ed365576930==%",base64_decode($requestd_data['key']));
    $ct=$a[0];
    $a= explode("^^001==$", $a[1]);
    $iv=$a[0];
    $s=$a[1];
    $encrypted=json_encode(array("ct"=>$ct,"iv"=>$iv,"s"=>$s));
    return Helper::cryptoJsAesDecrypt($encrypted);
}


}
