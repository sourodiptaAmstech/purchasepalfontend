<?php

namespace App\Helper;

use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class RestClient
{
    public static function curlRequest( $requestURL , $requestData ,$header, $requestType){
        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $requestURL);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $requestType);
            if (isset($requestData) && $requestData != '') {
            //  return $requestType; exit;
              curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_ENCODING, "");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    # required for https urls
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
            curl_setopt($ch, CURLOPT_TIMEOUT, 500);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);

            $info = curl_getinfo($ch);
            $objResponse = curl_exec($ch);
            if(curl_errno($ch)){
                return['error'=> curl_error($ch)];
            }
        } catch(Exception $e){
                return ['error' => $e];
        }
      return $objResponse;
  }
  public static function curlPrivacy( $requestURL , $requestData ,$header, $requestType){
    try{
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $requestURL);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $requestType);
      if (isset($requestData) && $requestData != '') {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
      }
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_ENCODING, "");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_AUTOREFERER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    # required for https urls
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
      curl_setopt($ch, CURLOPT_TIMEOUT, 500);
      curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
      
      $info = curl_getinfo($ch);
      $objResponse = curl_exec($ch);
      $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      
      if(curl_errno($ch)){
        return['error'=> curl_error($ch)];
      }
      return ['objResponse'=>$objResponse,"httpcode"=>$httpcode];
    }
    catch(Exception $e){
      return ['error' => $e];
    }
  }
}
