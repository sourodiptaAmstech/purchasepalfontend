<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', 'RegistrationServices@test');
//all api routes goes here
Route::get('/', 'PublicKeyServices@getPublicKey');
Route::post('funding/source/bank', 'FundingAccount\FundingController@addBank');
Route::post('create/card', 'Cards\AddCardsController@add');
Route::post('get/source/bank', 'FundingAccount\FundingController@getBank');


Route::group(['middleware' => ['ApiRequestValidation']], function () {
    Route::get('pre/registration/data', 'RegistrationServices@getPreRegistrationData');
    Route::get('country','RegistrationServices@country');
    Route::get('country/id','RegistrationServices@countrybyid');
    Route::get('state','RegistrationServices@state');
    Route::get('state/id','RegistrationServices@statebyid');
    Route::get('state/country/id','RegistrationServices@statebycountryid');
    Route::get('city','RegistrationServices@city');
    Route::get('city/id','RegistrationServices@citybyid');
    Route::get('city/state/id','RegistrationServices@citybystateid');
    Route::post('registeration','RegistrationServices@saveRegisterationData');
    Route::post('login','RegistrationServices@login');
    Route::get('profile','RegistrationServices@userProfile');
    Route::get('forgetpassword','RegistrationServices@forgetPassword');
    Route::post('forgetpassword','RegistrationServices@saveAndVerifyOtpForgetPassword');
    Route::put('changepassword','RegistrationServices@changepassword');
    Route::put('otpveificationemail','RegistrationServices@otpVeificationEmail');
});



